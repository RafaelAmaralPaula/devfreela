using DevFreela.Core.Entities;

namespace DevFreela.Infrastructure.Persistences
{
    public class DevFreelaContext
    {

        public List<Project> Projects { get; set; }

        public List<User> Users { get; set; }

        public List<Skill> Skills { get; set; }

        public DevFreelaContext()
        {
            Projects = new List<Project>{
                new Project("Meu projeto ASPNET Core 1" , "Minha descrição 1 " , 1 , 1 , 10000),
                new Project("Meu projeto ASPNET Core 2" , "Minha descrição 2 " , 1 , 1 , 20000),
                new Project("Meu projeto ASPNET Core 3" , "Minha descrição 3 " , 1 , 1 , 30000),
            };

            Users = new List<User>{
                new User("Luiz Felipe" , "luisdev@luisdev.com.br" , new DateTime(1992 , 1 , 1)),
                new User("Robert C Martin" , "robert@robert.com.br" , new DateTime(1950 , 1 , 1)),
                new User("Anderson" , "anderson@anderson.com.br" , new DateTime(1980 , 1 , 1)),
            };

            Skills = new List<Skill>{
                new Skill(".NET Core"),
                new Skill("C#"),
                new Skill("SQL")
            };
        }

    }
}