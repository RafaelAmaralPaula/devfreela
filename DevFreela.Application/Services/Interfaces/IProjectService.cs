using DevFreela.Application.InputModels;
using DevFreela.Application.ViewModels;

namespace DevFreela.Application.Services.Interfaces
{
    public interface IProjectService
    {
        List<ProjectViewModel> FindAll(string query);

        ProjectDetailsViewModel FindById(int id);

        ProjectDetailsViewModel Save(NewProjectInputModel inputModel);

        void Update(UpdateProjectInputModel inputModel);

        void Delete(int id);

        void CreateComment(CreateCommentInputModel inputModel);

        void Start(int id);

        void Finish(int id);

    }
}