using DevFreela.Application.InputModels;
using DevFreela.Application.Services.Interfaces;
using DevFreela.Application.ViewModels;
using DevFreela.Core.Entities;
using DevFreela.Infrastructure.Persistences;

namespace DevFreela.Application.Services.Implementations
{
    public class ProjectService : IProjectService
    {

        private readonly DevFreelaContext _dbContext;

        public ProjectService(DevFreelaContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<ProjectViewModel> FindAll(string query)
        {
            throw new NotImplementedException();
        }

        public ProjectDetailsViewModel FindById(int id)
        {
            throw new NotImplementedException();
        }

        public ProjectDetailsViewModel Save(NewProjectInputModel inputModel)
        {
            var project = new Project(inputModel.Title, inputModel.Description, inputModel.IdClient, inputModel.IdFreelancer, inputModel.TotalCost);
            _dbContext.Projects.Add(project);
        }

        public void Update(UpdateProjectInputModel inputModel)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }


        public void CreateComment(CreateCommentInputModel inputModel)
        {
            throw new NotImplementedException();
        }


        public void Start(int id)
        {
            throw new NotImplementedException();
        }

        public void Finish(int id)
        {
            throw new NotImplementedException();
        }

    }
}