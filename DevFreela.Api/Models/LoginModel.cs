namespace DevFreela.Api.Models
{
    public class LoginModel
    {



        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

    }
}