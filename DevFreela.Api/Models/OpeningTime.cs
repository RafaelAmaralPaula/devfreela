namespace DevFreela.Api.Models
{
    public class OpeningTime
    {

        public TimeSpan StartAt { get; set; }

        public TimeSpan FinishAt { get; set; }

    }
}