namespace DevFreela.Api.Models
{
    public class CreatedUserModel
    {

        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

    }
}