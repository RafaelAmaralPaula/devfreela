using DevFreela.Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace DevFreela.Api.Controllers
{
    [ApiController]
    [Route("/api/users")]
    public class UserController : ControllerBase
    {

        [HttpGet("{id}")]
        public IActionResult FindById(int id)
        {
            return Ok();
        }

        [HttpPost]
        public IActionResult Save([FromBody] CreatedUserModel createdUserModel)
        {
            return CreatedAtAction(nameof(FindById), new { id = createdUserModel.Id }, createdUserModel);
        }

        [HttpPut("{id}/login")]
        public IActionResult Update(int id, [FromBody] LoginModel logindModel)
        {
            return NoContent();
        }

    }
}