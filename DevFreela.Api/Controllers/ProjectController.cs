using DevFreela.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace DevFreela.Api.Controllers
{
    [ApiController]
    [Route("api/projects")]
    public class ProjectController : ControllerBase
    {

        private readonly OpeningTime _openingTime;

        public ProjectController(IOptions<OpeningTime> option)
        {
            _openingTime = option.Value;
        }

        [HttpGet]
        public IActionResult FindAll(string query)
        {
            return Ok();
        }

        [HttpGet("{id}")]
        public IActionResult FindById(int id)
        {
            return Ok();
        }

        [HttpPost]
        public IActionResult Save([FromBody] CreatedProjectModel createdProjectModel)
        {
            return CreatedAtAction(nameof(FindById), new { id = createdProjectModel.id }, createdProjectModel);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] UpdatedProjectModel updatedProjectModel)
        {
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return NoContent();
        }

        [HttpPost("{id}/comments")]
        public IActionResult SaveComment(int id, [FromBody] CreatedCommentModel createdCommentModel)
        {
            return NoContent();
        }

        [HttpPut("{id}/start")]
        public IActionResult Start(int id)
        {
            return NoContent();
        }

        [HttpPut("{id}/finish")]
        public IActionResult Finish(int id)
        {
            return NoContent();
        }

    }
}